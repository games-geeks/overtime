<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240320082758 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE historique_hs (id INT AUTO_INCREMENT NOT NULL, hs_id INT NOT NULL, date DATETIME NOT NULL, acteur VARCHAR(255) NOT NULL, profil VARCHAR(255) NOT NULL, action VARCHAR(255) NOT NULL, motif VARCHAR(255) DEFAULT NULL, INDEX IDX_FBE0C7D88FEAD8C1 (hs_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hs (id INT AUTO_INCREMENT NOT NULL, personne_id INT DEFAULT NULL, date_debut DATETIME DEFAULT NULL, heure_debut TIME DEFAULT NULL, date_fin DATETIME DEFAULT NULL, heure_fin TIME DEFAULT NULL, mois_annee VARCHAR(255) DEFAULT NULL, date_prev DATETIME DEFAULT NULL, motif VARCHAR(255) DEFAULT NULL, urgent VARCHAR(255) DEFAULT NULL, prime_p VARCHAR(255) DEFAULT NULL, appel_cadre VARCHAR(255) DEFAULT NULL, statut VARCHAR(255) DEFAULT NULL, reference VARCHAR(255) DEFAULT NULL, date_creation DATETIME NOT NULL, payee_recup VARCHAR(255) DEFAULT NULL, lien_d VARCHAR(255) DEFAULT NULL, INDEX IDX_25F1D3D6A21BD112 (personne_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE presence (id INT AUTO_INCREMENT NOT NULL, personne_id INT NOT NULL, date VARCHAR(255) DEFAULT NULL, mois_annee VARCHAR(255) NOT NULL, presence VARCHAR(255) NOT NULL, absence VARCHAR(255) NOT NULL, reference VARCHAR(255) NOT NULL, statut VARCHAR(255) DEFAULT NULL, commentaire LONGTEXT NOT NULL, total_presence DOUBLE PRECISION NOT NULL, total_absence DOUBLE PRECISION NOT NULL, total_maladie INT NOT NULL, total_teletravail INT NOT NULL, INDEX IDX_6977C7A5A21BD112 (personne_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE historique_hs ADD CONSTRAINT FK_FBE0C7D88FEAD8C1 FOREIGN KEY (hs_id) REFERENCES hs (id)');
        $this->addSql('ALTER TABLE hs ADD CONSTRAINT FK_25F1D3D6A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE presence ADD CONSTRAINT FK_6977C7A5A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
        $this->addSql('ALTER TABLE personne CHANGE cle cle VARCHAR(1) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE historique_hs DROP FOREIGN KEY FK_FBE0C7D88FEAD8C1');
        $this->addSql('ALTER TABLE hs DROP FOREIGN KEY FK_25F1D3D6A21BD112');
        $this->addSql('ALTER TABLE presence DROP FOREIGN KEY FK_6977C7A5A21BD112');
        $this->addSql('DROP TABLE historique_hs');
        $this->addSql('DROP TABLE hs');
        $this->addSql('DROP TABLE presence');
        $this->addSql('ALTER TABLE personne CHANGE cle cle VARCHAR(1) NOT NULL');
    }
}
