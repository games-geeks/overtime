<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240317201343 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE avoir DROP FOREIGN KEY FK_659B1A43275ED078');
        $this->addSql('DROP INDEX IDX_659B1A43275ED078 ON avoir');
        $this->addSql('DROP INDEX `primary` ON avoir');
        $this->addSql('ALTER TABLE avoir CHANGE profil_id service_id INT NOT NULL');
        $this->addSql('ALTER TABLE avoir ADD CONSTRAINT FK_659B1A43ED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id)');
        $this->addSql('CREATE INDEX IDX_659B1A43ED5CA9E6 ON avoir (service_id)');
        $this->addSql('ALTER TABLE avoir ADD PRIMARY KEY (role_id, personne_id, service_id)');
        $this->addSql('ALTER TABLE personne ADD profil_id INT NOT NULL');
        $this->addSql('ALTER TABLE personne ADD CONSTRAINT FK_FCEC9EF275ED078 FOREIGN KEY (profil_id) REFERENCES profil (id)');
        $this->addSql('CREATE INDEX IDX_FCEC9EF275ED078 ON personne (profil_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE avoir DROP FOREIGN KEY FK_659B1A43ED5CA9E6');
        $this->addSql('DROP INDEX IDX_659B1A43ED5CA9E6 ON avoir');
        $this->addSql('DROP INDEX `PRIMARY` ON avoir');
        $this->addSql('ALTER TABLE avoir CHANGE service_id profil_id INT NOT NULL');
        $this->addSql('ALTER TABLE avoir ADD CONSTRAINT FK_659B1A43275ED078 FOREIGN KEY (profil_id) REFERENCES profil (id)');
        $this->addSql('CREATE INDEX IDX_659B1A43275ED078 ON avoir (profil_id)');
        $this->addSql('ALTER TABLE avoir ADD PRIMARY KEY (role_id, profil_id, personne_id)');
        $this->addSql('ALTER TABLE personne DROP FOREIGN KEY FK_FCEC9EF275ED078');
        $this->addSql('DROP INDEX IDX_FCEC9EF275ED078 ON personne');
        $this->addSql('ALTER TABLE personne DROP profil_id');
    }
}
