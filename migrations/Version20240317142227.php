<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240317142227 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE avoir (role_id INT NOT NULL, profil_id INT NOT NULL, personne_id INT NOT NULL, INDEX IDX_659B1A43D60322AC (role_id), INDEX IDX_659B1A43275ED078 (profil_id), INDEX IDX_659B1A43A21BD112 (personne_id), PRIMARY KEY(role_id, profil_id, personne_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE avoir ADD CONSTRAINT FK_659B1A43D60322AC FOREIGN KEY (role_id) REFERENCES role (id)');
        $this->addSql('ALTER TABLE avoir ADD CONSTRAINT FK_659B1A43275ED078 FOREIGN KEY (profil_id) REFERENCES profil (id)');
        $this->addSql('ALTER TABLE avoir ADD CONSTRAINT FK_659B1A43A21BD112 FOREIGN KEY (personne_id) REFERENCES personne (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE avoir DROP FOREIGN KEY FK_659B1A43D60322AC');
        $this->addSql('ALTER TABLE avoir DROP FOREIGN KEY FK_659B1A43275ED078');
        $this->addSql('ALTER TABLE avoir DROP FOREIGN KEY FK_659B1A43A21BD112');
        $this->addSql('DROP TABLE avoir');
    }
}
