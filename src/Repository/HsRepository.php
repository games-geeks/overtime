<?php

namespace App\Repository;

use App\Entity\Hs;
use App\Entity\Personne;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Hs>
 *
 * @method Hs|null find($id, $lockMode = null, $lockVersion = null)
 * @method Hs|null findOneBy(array $criteria, array $orderBy = null)
 * @method Hs[]    findAll()
 * @method Hs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Hs::class);
    }

    public function getMaxId(Personne $personne): ?int
    {
        $query = $this->createQueryBuilder('s')
            ->select('s.reference, MAX(s.id) AS maxId')
            ->where('s.personne = :personne')->setParameter('personne', $personne)
            ->setMaxResults(1)
            ->groupBy('s.reference')
            ->orderBy('maxId', 'DESC');

        return $query->getQuery()->getOneOrNullResult();


        // "SELECT hs.id_hs,hs.reference FROM hs WHERE hs.id_hs = (SELECT MAX(hs.id_hs) 
        // 		FROM hs,personne WHERE personne.nom_prenom = '".$service->nomPrenom."' 
        // 		AND personne.id_personne = hs.id_personne) GROUP BY hs.reference");
    }

    //    /**
    //     * @return Hs[] Returns an array of Hs objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('h')
    //            ->andWhere('h.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('h.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Hs
    //    {
    //        return $this->createQueryBuilder('h')
    //            ->andWhere('h.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
