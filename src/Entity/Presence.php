<?php

namespace App\Entity;

use App\Repository\PresenceRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PresenceRepository::class)]
class Presence
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $date = null;

    #[ORM\Column(length: 255)]
    private ?string $moisAnnee = null;

    #[ORM\Column(length: 255)]
    private ?string $presence = null;

    #[ORM\Column(length: 255)]
    private ?string $absence = null;

    #[ORM\Column(length: 255)]
    private ?string $reference = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $statut = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $commentaire = null;

    #[ORM\Column]
    private ?float $totalPresence = null;

    #[ORM\Column]
    private ?float $totalAbsence = null;

    #[ORM\Column]
    private ?int $totalMaladie = null;

    #[ORM\Column]
    private ?int $totalTeletravail = null;

    #[ORM\ManyToOne(inversedBy: 'presences')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Personne $personne = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(?string $date): static
    {
        $this->date = $date;

        return $this;
    }

    public function getMoisAnnee(): ?string
    {
        return $this->moisAnnee;
    }

    public function setMoisAnnee(string $moisAnnee): static
    {
        $this->moisAnnee = $moisAnnee;

        return $this;
    }

    public function getPresence(): ?string
    {
        return $this->presence;
    }

    public function setPresence(string $presence): static
    {
        $this->presence = $presence;

        return $this;
    }

    public function getAbsence(): ?string
    {
        return $this->absence;
    }

    public function setAbsence(string $absence): static
    {
        $this->absence = $absence;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): static
    {
        $this->reference = $reference;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(?string $statut): static
    {
        $this->statut = $statut;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(string $commentaire): static
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getTotalPresence(): ?float
    {
        return $this->totalPresence;
    }

    public function setTotalPresence(float $totalPresence): static
    {
        $this->totalPresence = $totalPresence;

        return $this;
    }

    public function getTotalAbsence(): ?float
    {
        return $this->totalAbsence;
    }

    public function setTotalAbsence(float $totalAbsence): static
    {
        $this->totalAbsence = $totalAbsence;

        return $this;
    }

    public function getTotalMaladie(): ?int
    {
        return $this->totalMaladie;
    }

    public function setTotalMaladie(int $totalMaladie): static
    {
        $this->totalMaladie = $totalMaladie;

        return $this;
    }

    public function getTotalTeletravail(): ?int
    {
        return $this->totalTeletravail;
    }

    public function setTotalTeletravail(int $totalTeletravail): static
    {
        $this->totalTeletravail = $totalTeletravail;

        return $this;
    }

    public function getPersonne(): ?Personne
    {
        return $this->personne;
    }

    public function setPersonne(?Personne $personne): static
    {
        $this->personne = $personne;

        return $this;
    }
}
