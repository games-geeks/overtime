<?php

namespace App\Entity;

use App\Repository\AvoirRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AvoirRepository::class)]
class Avoir
{

    #[ORM\Id]
    #[ORM\ManyToOne(inversedBy: 'avoirs')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Role $role = null;

    #[ORM\Id]
    #[ORM\ManyToOne(inversedBy: 'avoirs')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Personne $personne = null;

    #[ORM\Id]
    #[ORM\ManyToOne(inversedBy: 'avoirs')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Service $service = null;

    private string $fullInfo = '';

    public function getRole(): ?Role
    {
        return $this->role;
    }

    public function setRole(?Role $role): static
    {
        $this->role = $role;

        return $this;
    }

    public function getPersonne(): ?Personne
    {
        return $this->personne;
    }

    public function setPersonne(?Personne $personne): static
    {
        $this->personne = $personne;

        return $this;
    }

    public function getService(): ?Service
    {
        return $this->service;
    }

    public function setService(?Service $service): static
    {
        $this->service = $service;

        return $this;
    }

    public function getFullInfo(): string
    {
        return $this->getService()->getNom() . ' - ' . $this->getRole()->getNom();
    }

    public function setFullInfo(): static
    {
        $this->fullInfo = $this->getService()->getNom() . ' - ' . $this->getRole()->getNom();
        return $this;
    }
}
