<?php

namespace App\Entity;

use App\Repository\HsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: HsRepository::class)]
class Hs
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateDebut = null;

    #[ORM\Column(type: Types::TIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $heureDebut = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateFin = null;

    #[ORM\Column(type: Types::TIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $heureFin = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $moisAnnee = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $datePrev = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $motif = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $urgent = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $primeP = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $appelCadre = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $statut = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $reference = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $dateCreation = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $payeeRecup = null;

    #[ORM\ManyToOne(inversedBy: 'hs')]
    private ?Personne $personne = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $lienD = null;

    #[ORM\OneToMany(targetEntity: HistoriqueHS::class, mappedBy: 'hs')]
    private Collection $historiqueHS;

    public function __construct()
    {
        $this->historiqueHS = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): static
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getHeureDebut(): ?\DateTimeInterface
    {
        return $this->heureDebut;
    }

    public function setHeureDebut(\DateTimeInterface $heureDebut): static
    {
        $this->heureDebut = $heureDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): static
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getHeureFin(): ?\DateTimeInterface
    {
        return $this->heureFin;
    }

    public function setHeureFin(?\DateTimeInterface $heureFin): static
    {
        $this->heureFin = $heureFin;

        return $this;
    }

    public function getMoisAnnee(): ?string
    {
        return $this->moisAnnee;
    }

    public function setMoisAnnee(?string $moisAnnee): static
    {
        $this->moisAnnee = $moisAnnee;

        return $this;
    }

    public function getDatePrev(): ?\DateTimeInterface
    {
        return $this->datePrev;
    }

    public function setDatePrev(?\DateTimeInterface $datePrev): static
    {
        $this->datePrev = $datePrev;

        return $this;
    }

    public function getMotif(): ?string
    {
        return $this->motif;
    }

    public function setMotif(?string $motif): static
    {
        $this->motif = $motif;

        return $this;
    }

    public function getUrgent(): ?string
    {
        return $this->urgent;
    }

    public function setUrgent(?string $urgent): static
    {
        $this->urgent = $urgent;

        return $this;
    }

    public function getPrimeP(): ?string
    {
        return $this->primeP;
    }

    public function setPrimeP(?string $primeP): static
    {
        $this->primeP = $primeP;

        return $this;
    }

    public function getAppelCadre(): ?string
    {
        return $this->appelCadre;
    }

    public function setAppelCadre(?string $appelCadre): static
    {
        $this->appelCadre = $appelCadre;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(?string $statut): static
    {
        $this->statut = $statut;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): static
    {
        $this->reference = $reference;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(\DateTimeInterface $dateCreation): static
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getPayeeRecup(): ?string
    {
        return $this->payeeRecup;
    }

    public function setPayeeRecup(?string $payeeRecup): static
    {
        $this->payeeRecup = $payeeRecup;

        return $this;
    }

    public function getPersonne(): ?Personne
    {
        return $this->personne;
    }

    public function setPersonne(?Personne $personne): static
    {
        $this->personne = $personne;

        return $this;
    }

    public function getLienD(): ?string
    {
        return $this->lienD;
    }

    public function setLienD(?string $lienD): static
    {
        $this->lienD = $lienD;

        return $this;
    }

    /**
     * @return Collection<int, HistoriqueHS>
     */
    public function getHistoriqueHS(): Collection
    {
        return $this->historiqueHS;
    }

    public function addHistoriqueH(HistoriqueHS $historiqueH): static
    {
        if (!$this->historiqueHS->contains($historiqueH)) {
            $this->historiqueHS->add($historiqueH);
            $historiqueH->setHs($this);
        }

        return $this;
    }

    public function removeHistoriqueH(HistoriqueHS $historiqueH): static
    {
        if ($this->historiqueHS->removeElement($historiqueH)) {
            // set the owning side to null (unless already changed)
            if ($historiqueH->getHs() === $this) {
                $historiqueH->setHs(null);
            }
        }

        return $this;
    }
}
