<?php

namespace App\DataFixtures;

use App\Entity\Profil;
use App\Entity\Role;
use App\Entity\Service;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $roles =  ['Demandeur', 'Valideur', 'Vérificateur', 'Approbateur', 'RH'];
        foreach ($roles as $role) {
            $theRole = (new Role)->setNom($role);
            $manager->persist($theRole);
        }
        $profils = ['Agent', 'Responsable de service', 'Direction', 'RH'];
        foreach ($profils as $profil) {
            $theProfil = (new Profil)->setNom($profil);
            $manager->persist($theProfil);
        }
        $services = ['DOTF/RSCD', 'DOTF/INT', 'DOTF/EXP', 'LOGISTIQUE', 'SI2L', 'RH'];
        foreach ($services as $service) {
            $theService = (new Service)->setNom($service);
            $manager->persist($theService);
        }

        $manager->flush();
    }
}
