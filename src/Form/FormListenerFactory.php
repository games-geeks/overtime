<?php

namespace App\Form;

use App\Entity\Personne;
use Symfony\Component\Form\Event\PreSubmitEvent;
use Symfony\Component\Form\Event\PostSubmitEvent;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class FormListenerFactory
{

    public function __construct(private SluggerInterface $slugger, private readonly UserPasswordHasherInterface $userPasswordHasher)
    {
    }

    // public function autoSlug(string $field): callable
    // {

    //     return function (PreSubmitEvent $event) use ($field) {
    //         $data = $event->getData();
    //         if (empty($data['slug'])) {
    //             $data['slug'] = strtolower($this->slugger->slug($data[($field)]));
    //             $event->setData($data);
    //         }
    //     };
    // }

    public function autoMDP(): callable
    {

        return function (PostSubmitEvent $event) {
            $data = $event->getData();
            if (!$data instanceof Personne)
                return;
            if (!$data->getPassword()) {
                $data->setPassword($this->userPasswordHasher->hashPassword($data, $data->getIdentifiant()));
            }
        };
    }


    public function timestamps(): callable
    {
        return function (PostSubmitEvent $event) {
            $data = $event->getData();

            $data->setUpdatedAt(new \DateTimeImmutable());
            if (!$data->getId())
                $data->setCreatedAt(new \DateTimeImmutable());
        };
    }
}
