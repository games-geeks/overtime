<?php

namespace App\Form;

use App\Entity\Avoir;
use App\Entity\Profil;
use App\Entity\Personne;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class PersonneType extends AbstractType
{

    public function __construct(private FormListenerFactory $flf)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'attr' => array(
                    'placeholder' => '@',
                    'style' => 'text-align:center'
                )
            ])
            // ->add('roles')
            // ->add('password')
            ->add('nomPrenom', TextType::class, [
                'attr' => array(
                    'placeholder' => 'DUPOND Jean',
                    'style' => 'text-align:center'
                )
            ])
            ->add('identifiant', IntegerType::class, [
                'attr' => array(
                    'placeholder' => '20110',
                    'style' => 'text-align:center'
                )
            ])
            // ->add('cle')
            ->add('trigramme', TextType::class, [
                'attr' => array(
                    'placeholder' => 'JDU',
                    'style' => 'text-align:center'
                )
            ])
            ->add(
                'profil',
                EntityType::class,
                [
                    'class' => Profil::class,
                    'choice_label' => 'nom',
                    'placeholder' => 'Profil',
                ]
            )
            ->add('save', SubmitType::class, [
                'label' => 'Envoyer',
                'attr' => array(
                    'class' => "btn btn-outline-primary profilHab",
                    'style' => 'text-align:center'
                )
            ])
            ->add(
                'avoirs',
                CollectionType::class,
                [
                    'entry_type' => AvoirType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'entry_options' => ['label' => false],
                    'attr' => [
                        'data-controller' => 'form-collection'
                    ]
                ]
            )
            ->addEventListener(FormEvents::POST_SUBMIT, $this->flf->autoMDP());

        // if ($options['nameController'] == 'addHabilitation') {

        //     $builder->add(
        //         'avoirs',
        //         CollectionType::class,
        //         [
        //             'entry_type' => AvoirType::class,
        //         ]
        //     );
        // }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Personne::class,
        ]);
    }
}
