<?php

namespace App\Form;

use App\Entity\Role;
use App\Entity\Avoir;
use App\Entity\Service;
use App\Entity\Personne;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AvoirType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if ($options['nameController'] == 'avoir') {
            $builder
                ->add('role', EntityType::class, [
                    'class' => Role::class,
                    'choice_label' => 'nom',
                    'placeholder' => 'Role',
                ])
                // ->add('personne', EntityType::class, [
                //     'class' => Personne::class,
                //     'choice_label' => 'nomPrenom',
                // ])
                ->add('service', EntityType::class, [
                    'class' => Service::class,
                    'choice_label' => 'nom',
                    'placeholder' => 'Service',
                ])
                ->add('save', SubmitType::class, [
                    'label' => 'Envoyer',
                    'attr' => array(
                        'class' => "btn btn-outline-primary profilHab",
                        'style' => 'text-align:center'
                    )
                ]);
        } else
            $builder->add('fullInfo', TextType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'nameController' => '',
            'data_class' => Avoir::class,
        ]);

        $resolver->setAllowedTypes('nameController', 'string');
    }
}
