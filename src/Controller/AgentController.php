<?php

namespace App\Controller;

use App\Entity\Personne;
use App\Form\PersonneType;
use App\Repository\PersonneRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route("/agent", name: 'agent.')]
class AgentController extends AbstractController
{
    #[Route('/menu', name: 'menu')]
    public function index(): Response
    {
        return $this->render('agent/menu.html.twig', [
            'controller_name' => 'MenuController',
        ]);
    }

    #[Route('/ajout', name: 'ajouter')]
    public function ajouter(Request $request, EntityManagerInterface $em): Response
    {

        $personne =  new Personne;
        $form = $this->createForm(PersonneType::class, $personne);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($personne);
            $em->flush();
            $this->addFlash('success', 'La personne a été créée');
            return $this->redirectToRoute('agent.gestion');
        }

        return $this->render('agent/ajout.html.twig', [
            'form' => $form,
        ]);
    }

    // #[Route('/gestion', name: 'gestion')]
    // public function gestion(): Response
    // {
    //     return $this->render('rh/gestionHabilitation.html.twig', [
    //         'controller_name' => 'AgentController',
    //     ]);
    // }

    #[Route('/liste_demande', name: 'liste')]
    public function listeDemande(): Response
    {
        return $this->render('agent/listeDemande.html.twig', [
            'controller_name' => 'AgentController',
        ]);
    }

    #[Route('/modification', name: 'modifier')]
    public function modification(Request $request, PersonneRepository $pr, EntityManagerInterface $em): Response
    {

        $idP = $request->query->getInt('id', 0);
        $personnes = $pr->findby([], orderBy: ['nomPrenom' => 'ASC']);
        $form = $this->createForm(PersonneType::class);
        if ($idP > 0) {
            $personne = $pr->findOneBy(['id' => $idP]);
            $form = $this->createForm(PersonneType::class, $personne);
            $form->handleRequest($request);
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($personne);
            $em->flush();
            $this->addFlash('success', 'La personne a été modifiée');
            return $this->redirectToRoute('rh.gestion');
        }

        return $this->render('agent/modification.html.twig', compact('personnes', 'form'));
    }

    #[Route('/suppression', name: 'supprimer')]
    public function suppression(Request $request, PersonneRepository $pr, EntityManagerInterface $em): Response
    {

        $idP = $request->query->getInt('id', 0);
        $personnes = $pr->findby([], orderBy: ['nomPrenom' => 'ASC']);
        $form = $this->createForm(PersonneType::class);
        if ($idP > 0) {
            $personne = $pr->findOneBy(['id' => $idP]);
            $form = $this->createForm(PersonneType::class, $personne);
            $form->handleRequest($request);
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($personne);
            $em->persist($personne);
            // $em->flush();
            $this->addFlash('success', 'La personne a été supprimée');
            return $this->redirectToRoute('rh.gestion');
        }

        return $this->render('agent/suppression.html.twig', compact('personnes', 'form'));
    }
}
