<?php

namespace App\Controller;

use App\Entity\Personne;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use function PHPUnit\Framework\throwException;

class MenuController extends AbstractController
{
    #[Route('/', name: 'menu')]
    public function index(): Response
    {
        $user = $this->getUser();

        if (!$user)
            return $this->redirectToRoute('app_login');
        switch ($user->getProfil()->getId()) {
            case 1:
                return $this->render('agent/menu.html.twig', [
                    'controller_name' => 'MenuController',
                ]);
                break;
            case 2:
                return $this->render('responsable/menu.html.twig', [
                    'controller_name' => 'MenuController',
                ]);
                break;
            case 3:
                return $this->render('direction/menu.html.twig', [
                    'controller_name' => 'MenuController',
                ]);
                break;
            case 4:
                return $this->render('rh/menu.html.twig', [
                    'controller_name' => 'MenuController',
                ]);
                break;
            default:
                throw ("Le profil n'est pas le bon");
                break;
        }
    }
}
