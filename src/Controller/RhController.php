<?php

namespace App\Controller;

use App\Entity\Avoir;
use App\Entity\Personne;
use App\Entity\Service;
use App\Form\AvoirType;
use App\Form\ServiceType;
use App\Form\PersonneType;
use App\Repository\AvoirRepository;
use App\Repository\ServiceRepository;
use App\Repository\PersonneRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route("/rh", name: 'rh.')]
class RhController extends AbstractController
{
    #[Route('/menu', name: 'menu')]
    public function index(): Response
    {
        return $this->render('rh/menu.html.twig', [
            'controller_name' => 'MenuController',
        ]);
    }



    #[Route('/gestion', name: 'gestion')]
    public function gestion(): Response
    {
        return $this->render('rh/gestionHabilitation.html.twig', [
            'controller_name' => 'AgentController',
        ]);
    }

    #[Route('/ajoutHab', name: 'hab.ajout')]
    public function ajoutHab(
        Request $request,
        PersonneRepository $pr,
        EntityManagerInterface $em,
    ): Response {

        $idP = $request->query->getInt('id', 0);
        $personnes = $pr->findby([], orderBy: ['nomPrenom' => 'ASC']);
        $avoir = new Avoir;
        $form = $this->createForm(PersonneType::class);
        $formAvoir = $this->createForm(AvoirType::class, $avoir,  [
            'nameController' => 'avoir'
        ]);
        if ($idP > 0) {
            $personne = $pr->findOneBy(['id' => $idP]);
            $form = $this->createForm(PersonneType::class, $personne);
            $formAvoir->handleRequest($request);
        }
        if ($formAvoir->isSubmitted() && $formAvoir->isValid()) {
            $avoir->setPersonne($personne);
            $em->persist($avoir);
            $em->flush();
            $this->addFlash('success', 'L\'habiliation a bien été ajoutée');
            return $this->redirectToRoute('rh.hab.ajout', ['id' => $idP]);
        }

        return $this->render('rh/ajoutHabilitation.html.twig', [
            'personnes' => $personnes,
            'form' => $form,
            'formAvoir' => $formAvoir,
        ]);
    }

    #[Route('/ajoutService', name: 'service.ajout')]
    public function ajoutService(Request $request, ServiceRepository $sr, EntityManagerInterface $em): Response
    {
        $services = $sr->findby([], orderBy: ['nom' => 'ASC']);
        $service = new Service;
        $form = $this->createForm(ServiceType::class, $service);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($service);
            $em->flush();
            $this->addFlash('success', 'Le service a bien été ajouté');
            return $this->redirectToRoute('rh.service.ajout');
        }

        return $this->render('rh/ajoutService.html.twig', compact('services', 'form'));
    }
}
