<?php

namespace App\Controller;

use App\Repository\HsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route("/demande", name: 'demande.')]
class DemandeController extends AbstractController
{
    #[Route('/liste', name: 'liste')]
    public function index(): Response
    {
        return $this->render('demande/liste.html.twig', [
            'controller_name' => 'DemandeController',
        ]);
    }

    #[Route('/nouvelle_hs', name: 'nouvelle.hs')]
    public function nouvelleHS(HsRepository $hr): Response
    {
        $reference = $hr->getMaxId($this->getUser());
        if (!$reference) // on n'a pas encore de référence
        {
            $today = date("Ymd");
            $occurence = 1;
            $HS = 'HS';
            $reference = $HS . "-" . $this->getUser()->getTrigramme() . "-" . $today . "-" . $occurence;
        }
        // dd($reference);
        // if($reference2 !== NULL)
        // {
        //     $occurence2 = explode("-", $reference2);
        //     $occurence3 = $occurence2[3]+1;
        //     $today = date("Ymd");
        //     $today2 = date("d-m-Y");
        //     $HS = 'HS';
        //     $reference = $HS."-".$AfficheProfil['trigramme']."-".$today."-".$occurence3; 
        // }
        // else
        // {
        //     $today = date("Ymd");
        //     $today2 = date("d-m-Y");
        //     $occurence = 0;
        //     $occurence = $occurence + 1;
        //     $HS = 'HS';
        //     $reference = $HS."-".$AfficheProfil['trigramme']."-".$today."-".$occurence;
        // }



        return $this->render('demande/hs.html.twig', [
            'reference' => $reference,
        ]);
    }
}
