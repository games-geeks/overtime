import './bootstrap.js';
/*
 * Welcome to your app's main JavaScript file!
 *
 * This file will be included onto the page via the importmap() Twig function,
 * which should already be in your base.html.twig.
 */
import './styles/app.css';
import './styles/style.css';
import './styles/style2.css';
import './styles/hs.css';
import './styles/font-awesome.min.css';

console.log('This log comes from assets/app.js - welcome to AssetMapper! 🎉');
