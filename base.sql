-- phpMyAdmin SQL Dump
-- version 4.6.0

-- Version du serveur :  5.5.48
-- Version de PHP :  5.5.35

--
-- Base de données :  `hs_astreinte`
--

-- --------------------------------------------------------

--
-- Structure de la table `astreinte`
--

CREATE TABLE `astreinte` (
  `id_a` int(9) NOT NULL,
  `date_deplacdeb` date NOT NULL,
  `date_deplacfin` date NOT NULL,
  `date_deb` date DEFAULT NULL,
  `heure_deb` time DEFAULT NULL,
  `date_fin` date DEFAULT NULL,
  `heure_fin` time DEFAULT NULL,
  `adresse` varchar(150) CHARACTER SET latin1 DEFAULT NULL,
  `motif` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `km` float DEFAULT NULL,
  `montant` float DEFAULT NULL,
  `repasm` float DEFAULT NULL,
  `nbplage` int(3) DEFAULT NULL,
  `indem` float DEFAULT NULL,
  `totalM` float DEFAULT NULL,
  `totalR` float DEFAULT NULL,
  `totalP` float NOT NULL,
  `totalI` float NOT NULL,
  `date_creation` date NOT NULL,
  `statut` varchar(40) CHARACTER SET latin1 NOT NULL,
  `reference` varchar(30) CHARACTER SET latin1 NOT NULL,
  `lien_d` varchar(30) DEFAULT NULL,
  `id_personne` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Structure de la table `avoir`
--

-- CREATE TABLE `avoir` (
--   `id_r` int(5) NOT NULL,
--   `id_personne` int(5) NOT NULL,
--   `id_s` int(5) NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Structure de la table `historiqueAstreinte`
--

CREATE TABLE `historiqueAstreinte` (
  `id_histo` int(9) NOT NULL,
  `date` date NOT NULL,
  `acteur` varchar(150) NOT NULL,
  `profil` varchar(30) NOT NULL,
  `action` varchar(70) NOT NULL,
  `motif` varchar(275) DEFAULT NULL,
  `id_a` int(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Structure de la table `hs`
--

-- CREATE TABLE `hs` (
--   `id_hs` int(9) NOT NULL,
--   `date_deb` date DEFAULT NULL,
--   `heure_deb` time DEFAULT NULL,
--   `date_fin` date DEFAULT NULL,
--   `heure_fin` time DEFAULT NULL,
--   `mois_annee` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
--   `date_prev` date DEFAULT NULL,
--   `motif` varchar(150) CHARACTER SET latin1 DEFAULT NULL,
--   `urgent` varchar(5) CHARACTER SET latin1 DEFAULT NULL,
--   `primep` varchar(5) CHARACTER SET latin1 DEFAULT NULL,
--   `appel_cadre` varchar(5) DEFAULT NULL,
--   `statut` varchar(40) CHARACTER SET latin1 DEFAULT NULL,
--   `reference` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
--   `lien_d` varchar(30) DEFAULT NULL,
--   `date_creation` date DEFAULT NULL,
--   `payee_recup` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
--   `id_personne` int(5) NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Structure de la table `param_tech`
--

CREATE TABLE `param_tech` (
  `id_param` int(3) NOT NULL,
  `valeur_plage_astr` float NOT NULL,
  `date_valeurP` date NOT NULL,
  `valeur_plage_astr2` float NOT NULL,
  `date_valeurP2` date NOT NULL,
  `date_purge_astr` date NOT NULL,
  `date_purge_hs` date NOT NULL,
  `adresse_messagerie` varchar(50) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Structure de la table `personne`
--

-- CREATE TABLE `personne` (
--   `id_personne` int(11) NOT NULL,
--   `nom_prenom` varchar(150) NOT NULL,
--   `identifiant` int(5) NOT NULL,
--   `cle` varchar(1) DEFAULT NULL,
--   `mail` varchar(200) NOT NULL,
--   `trigramme` varchar(3) NOT NULL,
--   `id_profil` int(5) NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Structure de la table `presence`
--

-- CREATE TABLE `presence` (
--   `id` int(11) NOT NULL,
--   `date` varchar(50) DEFAULT NULL,
--   `mois_annee` varchar(15) NOT NULL,
--   `presence` varchar(15) NOT NULL,
--   `absence` varchar(15) NOT NULL,
--   `reference` varchar(13) NOT NULL,
--   `statut` varchar(30) DEFAULT NULL,
--   `id_personne` int(5) NOT NULL,
--   `commentaire` varchar(300) NOT NULL,
--   `total_presence` float NOT NULL,
--   `total_absence` float NOT NULL,
--   `total_maladie` int(2) NOT NULL,
--   `total_teletravail` int(2) NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=latin1;



-- --
-- -- Structure de la table `profil`
-- --

-- CREATE TABLE `profil` (
--   `id_profil` int(5) NOT NULL,
--   `nom_profil` varchar(25) CHARACTER SET latin1 NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --
-- -- Contenu de la table `profil`
-- --

-- INSERT INTO `profil` (`id_profil`, `nom_profil`) VALUES
-- (1, 'Agent'),
-- (2, 'Responsable de service'),
-- (3, 'Direction'),
-- (4, 'RH');

-- -- --------------------------------------------------------

--
-- Structure de la table `role`
--

-- CREATE TABLE `role` (
--   `id_r` int(5) NOT NULL,
--   `nom_role` varchar(15) CHARACTER SET latin1 NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --
-- -- Contenu de la table `role`
-- --

-- INSERT INTO `role` (`id_r`, `nom_role`) VALUES
-- (1, 'Demandeur'),
-- (2, 'Valideur'),
-- (3, 'Vérificateur'),
-- (4, 'Approbateur'),
-- (5, 'RH');

-- -- --------------------------------------------------------

--
-- Structure de la table `service`
--

-- CREATE TABLE `service` (
--   `id_s` int(5) NOT NULL,
--   `nom_service` varchar(30) CHARACTER SET latin1 NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --
-- -- Contenu de la table `service`
-- --


--
-- Index pour les tables exportées
--

--
-- Index pour la table `astreinte`
--
ALTER TABLE `astreinte`
  ADD PRIMARY KEY (`id_a`),
  ADD KEY `id_personne` (`id_personne`);

--
-- Index pour la table `avoir`
--
ALTER TABLE `avoir`
  ADD PRIMARY KEY (`id_r`,`id_personne`,`id_s`),
  ADD KEY `id_s` (`id_s`),
  ADD KEY `id_personne` (`id_personne`);

--
-- Index pour la table `historiqueAstreinte`
--
ALTER TABLE `historiqueAstreinte`
  ADD PRIMARY KEY (`id_histo`),
  ADD KEY `id_a` (`id_a`);

--
-- Index pour la table `historiqueHS`
--
ALTER TABLE `historiqueHS`
  ADD PRIMARY KEY (`id_histo`),
  ADD KEY `id_hs` (`id_hs`);

--
-- Index pour la table `hs`
--
ALTER TABLE `hs`
  ADD PRIMARY KEY (`id_hs`),
  ADD KEY `id_personne` (`id_personne`);

--
-- Index pour la table `param_tech`
--
ALTER TABLE `param_tech`
  ADD PRIMARY KEY (`id_param`);

--
-- Index pour la table `personne`
--
ALTER TABLE `personne`
  ADD PRIMARY KEY (`id_personne`),
  ADD KEY `id_profil` (`id_profil`);

--
-- Index pour la table `presence`
--
ALTER TABLE `presence`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_personne` (`id_personne`);

--
-- Index pour la table `profil`
--
ALTER TABLE `profil`
  ADD PRIMARY KEY (`id_profil`);

--
-- Index pour la table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_r`);

--
-- Index pour la table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id_s`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `astreinte`
--
ALTER TABLE `astreinte`
  MODIFY `id_a` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
--
-- AUTO_INCREMENT pour la table `historiqueAstreinte`
--
ALTER TABLE `historiqueAstreinte`
  MODIFY `id_histo` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
--
-- AUTO_INCREMENT pour la table `historiqueHS`
--
ALTER TABLE `historiqueHS`
  MODIFY `id_histo` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
--
-- AUTO_INCREMENT pour la table `hs`
--
ALTER TABLE `hs`
  MODIFY `id_hs` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
--
-- AUTO_INCREMENT pour la table `param_tech`
--
ALTER TABLE `param_tech`
  MODIFY `id_param` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
--
-- AUTO_INCREMENT pour la table `personne`
--
ALTER TABLE `personne`
  MODIFY `id_personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
--
-- AUTO_INCREMENT pour la table `presence`
--
ALTER TABLE `presence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
--
-- AUTO_INCREMENT pour la table `profil`
--
ALTER TABLE `profil`
  MODIFY `id_profil` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
--
-- AUTO_INCREMENT pour la table `role`
--
ALTER TABLE `role`
  MODIFY `id_r` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
--
-- AUTO_INCREMENT pour la table `service`
--
ALTER TABLE `service`
  MODIFY `id_s` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `astreinte`
--
ALTER TABLE `astreinte`
  ADD CONSTRAINT `astreinte_ibfk_1` FOREIGN KEY (`id_personne`) REFERENCES `personne` (`id_personne`);

--
-- Contraintes pour la table `avoir`
--
ALTER TABLE `avoir`
  ADD CONSTRAINT `avoir_ibfk_1` FOREIGN KEY (`id_s`) REFERENCES `service` (`id_s`),
  ADD CONSTRAINT `avoir_ibfk_2` FOREIGN KEY (`id_r`) REFERENCES `role` (`id_r`),
  ADD CONSTRAINT `avoir_ibfk_3` FOREIGN KEY (`id_personne`) REFERENCES `personne` (`id_personne`);

--
-- Contraintes pour la table `historiqueAstreinte`
--
ALTER TABLE `historiqueAstreinte`
  ADD CONSTRAINT `historiqueAstreinte_ibfk_1` FOREIGN KEY (`id_a`) REFERENCES `astreinte` (`id_a`);

--
-- Contraintes pour la table `historiqueHS`
--
ALTER TABLE `historiqueHS`
  ADD CONSTRAINT `historiqueHS_ibfk_1` FOREIGN KEY (`id_hs`) REFERENCES `hs` (`id_hs`);

--
-- Contraintes pour la table `hs`
--
ALTER TABLE `hs`
  ADD CONSTRAINT `hs_ibfk_1` FOREIGN KEY (`id_personne`) REFERENCES `personne` (`id_personne`);

--
-- Contraintes pour la table `personne`
--
ALTER TABLE `personne`
  ADD CONSTRAINT `personne_ibfk_1` FOREIGN KEY (`id_profil`) REFERENCES `profil` (`id_profil`);

--
-- Contraintes pour la table `presence`
--
ALTER TABLE `presence`
  ADD CONSTRAINT `presence_ibfk_1` FOREIGN KEY (`id_personne`) REFERENCES `personne` (`id_personne`);

